// var workExperience = [
//         {
//           id: 0,
//           title: 'Research Intern',
//           subtitle: 'Mayo Clinic, Phoenix, Arizona',
//           duration: 'July 2018 - Present',
//           bullets: ["Developed augmented reality medical application for Microsoft HoloLens using C# and Unity",
//           "Coordinated with project team to manage priorities, goals, and feature implementation for final product"]
//         },
//         {
//           id: 1,
//           title: 'Website Content Assistant',
//           subtitle: 'EdPlus at Arizona State University, Scottsdale, Arizona',
//           duration: 'February 2018 - Present',
//           bullets: ["Improved website visuals and functionality by updating HTML and JavaScript",
//           "Created and edited graphic design collateral in Photoshop and Illustrator to maintain brand image",
//           "Assisted in administrative tasks such as consolidating and visualizing financial data",
//           "Supported clients in developing online courses by leveraging learning management system knowledge"]
//         }
//       ];
//
// var technicalProjects = [
//         {
//           id: 2,
//           title: 'Pragma Technologies Company Website | CIS300 Web Design and Development Honors Project',
//           subtitle: 'https://jtjoe.github.io/pragma-tech/',
//           duration: '',
//           bullets: ["Designed and built website for fictitious company using HTML, CSS, and Vue.js",
//           "Developed brand image by establishing company branding, authoring copy text, and curating visuals"]
//         },
// ];
//
// var extracurriculars = [
//         {
//           id: 3,
//           title: "Software Developer's Association",
//           subtitle: 'Digital Media Co-Director',
//           duration: "August 2017 - Present",
//           bullets: ["Coordinated with officers to produce promotional social media content using Adobe InDesign",
//           "Attended meetings to explore new technologies and improve programming skills"]
//         },
//         {
//           id: 4,
//           title: 'SunHacks 2018',
//           subtitle: 'Volunteer',
//           duration: 'April 6 - April 8, 2018',
//           bullets: ["Managed hardware lab by organizing rental logistics and answering participant questions",
//           "Attended workshops to improve programming skills and learn new technologies"]
//         }
// ];
//
// Vue.component('to-top',{ //for back to top buttons
//   template:'<p><a href="#">back to top</a></p>'
// });
//
// Vue.component('content-item', {
//   props: {
//     title: String,
//     subtitle: String
//   },
//   template:
//     '<div class="text"> <h3>{{exp.title}}</h3> <p>{{exp.subtitle}}</p><br> <p class="right-align">{{exp.duration}}</p> </div>',
//     data() {
//       return {
//
//       }
//     },
//     // <ul><li v-for="bullet in content.bullets"></li></ul>
// })
//
//
// new Vue({
//     el: 'content',
//     data: {
//       input: [workExperience, technicalProjects, extracurriculars]
//     },
//
//     // mounted: function () {
//     //
//     // },
//     //
//     // methods: {
//     //
//     // },
//     //
//     // computed: {
//     //
//     // }
// });
