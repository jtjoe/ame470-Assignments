# Assignment 2

**Due Feb 6<sup>th</sup> before class**

## Task

Starting with the code handout write a servers a single page resume.
It should have at-least 4 sections

0. Name + thumbnail (phone, address and email are links)
1. Objective
2. Education (use table)
3. Experience (use one paragraph for each entry -- ordered list)
4. References (unordered list)

## Submisssion
Demo running server (http://3.17.141.210:8080/index.html).
