# Assignment 4

**Due Feb 25<sup>th</sup> before class**

## Task

Starting with the code handout implement the <u>delete todo</u>
functionality (using mongoDB).

## Submisssion
Demo running server (http://3.17.141.210).

## References

https://docs.mongodb.com/manual/reference/method/db.collection.remove/

https://gist.github.com/fengmk2/1194742
